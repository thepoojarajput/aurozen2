package aurozen.client.updation;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="employees")
public class Employees {

	@Id
	@GenericGenerator(
			name="employees_sequence",
			strategy = "aurozen.client.updation.EmployeesIdGenerator")
	@GeneratedValue(generator = "employees_sequence")
	@Column(name = "emp_id")
	private String empId;
	
	@NotNull
	private String empName;
	
	@NotNull
	@Size(min = 10, max = 10)
	@Column(unique = true)
	private String empContact;
	
	@NotNull
	private String empEmail;
	
	@NotNull
	private String empPhoto;
	
	private String empPass;
	
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "emp_id", referencedColumnName = "emp_id")
	private List<Skills> skillSet;
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpContact() {
		return empContact;
	}
	public void setEmpContact(String empContact) {
		this.empContact = empContact;
	}
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	public String getEmpPhoto() {
		return empPhoto;
	}
	public void setEmpPhoto(String empPhoto) {
		this.empPhoto = empPhoto;
	}
	public List<Skills> getSkillSet() {
		return skillSet;
	}
	public void setSkillSet(List<Skills> skillSet) {
		this.skillSet = skillSet;
	}
	public String getEmpPass() {
		return empPass;
	}
	public void setEmpPass(String empPass) {
		this.empPass = empPass;
	}
}
