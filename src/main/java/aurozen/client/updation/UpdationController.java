package aurozen.client.updation;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aurozen.client.response.Response;
import aurozen.client.response.ResponseData;
import aurozen.client.response.ResponseList;

@RestController
@RequestMapping("/api")
public class UpdationController {
	
	@Autowired
	private UpdationService service;
	
	@GetMapping("/employees/{empId}")
	public ResponseData getEmployeeById(@PathVariable String empId) {
		
		return service.getEmployeeById(empId);
	}
	

	@PutMapping("/employees")
	public ResponseData updateEmployeeById(@RequestBody@Valid Employees employees, BindingResult result) {
		
		return service.updateEmployeeById(employees, result);
	}

}
