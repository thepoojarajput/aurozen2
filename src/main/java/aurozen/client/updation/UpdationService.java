package aurozen.client.updation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import aurozen.client.response.ResponseData;

@Service
public class UpdationService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	public ResponseData getEmployeeById(String empId) {
		ResponseData response = new ResponseData();
		
		Optional<Employees> optional = employeeRepository.findById(empId);
		
		
		if(optional.isPresent()) {

			response.setMessage("Success!");
			response.setResponse(true);
			optional.ifPresent(emp ->  response.setData( emp));
			
		}else {

			response.setMessage("No Data Found!");
			response.setResponse(false);
			response.setCode(404);
		}
		
		return response;
	}
	
	public ResponseData updateEmployeeById(Employees employees, BindingResult result) {
		
		ResponseData response = new ResponseData();
		
		if(!result.hasErrors()) {
			
			employees = employeeRepository.save(employees);
			
			if(employees != null) {
				response.setMessage("Successfully Updated!");
				response.setResponse(true);
			}
		}else {
			System.out.println(result.getAllErrors());
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError objectError : errors) {
				System.out.println("@" + objectError.getField().toUpperCase() + ":" + objectError.getDefaultMessage());
			}
			response.setMessage("Error With Your Data!");
			response.setResponse(false);
			response.setCode(500);
		}
		return response;
	}
}
